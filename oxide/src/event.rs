pub use glutin::event::{MouseButton, MouseScrollDelta, VirtualKeyCode};
use glutin::{
    dpi::{PhysicalPosition, PhysicalSize},
    event::{ElementState, KeyboardInput, ModifiersState, ScanCode},
};
use std::convert::TryFrom;

#[derive(Debug)]
pub enum Event {
    Window(WindowEvent),
    Input(InputEvent),
}

#[derive(Debug)]
pub enum WindowEvent {
    Resized(PhysicalSize<u32>),
    Moved(PhysicalPosition<i32>),
    ScaleFactorChanged(f64),

    CloseRequested,

    FocusLost,
    FocusGained,

    Suspended,
    Resumed,
}

#[derive(Debug)]
pub enum InputEvent {
    Keyboard(KeyboardEvent),
    Mouse(MouseEvent),
}

#[derive(Debug)]
pub enum KeyboardEvent {
    Pressed(ScanCode, Option<VirtualKeyCode>),
    Released(ScanCode, Option<VirtualKeyCode>),

    ReceivedCharacter(char),

    ModifiersChanged(ModifiersState),
}

#[derive(Debug)]
pub enum MouseEvent {
    Pressed(MouseButton),
    Released(MouseButton),

    Moved(PhysicalPosition<f64>),

    MouseWheel(MouseScrollDelta),

    CursorLeft,
    CursorEntered,
}

impl<E> TryFrom<&glutin::event::Event<'_, E>> for Event {
    type Error = ();

    fn try_from(event: &glutin::event::Event<E>) -> Result<Self, Self::Error> {
        use glutin::event::{Event as WinitEvent, WindowEvent as WinitWindowEvent};
        match event {
            WinitEvent::WindowEvent { event, .. } => match event {
                WinitWindowEvent::Resized(size) => Ok(Self::Window(WindowEvent::Resized(*size))),
                WinitWindowEvent::Moved(pos) => Ok(Self::Window(WindowEvent::Moved(*pos))),
                WinitWindowEvent::ScaleFactorChanged { scale_factor, .. } => {
                    Ok(Self::Window(WindowEvent::ScaleFactorChanged(*scale_factor)))
                }
                WinitWindowEvent::CloseRequested => Ok(Self::Window(WindowEvent::CloseRequested)),
                WinitWindowEvent::Focused(state) => match state {
                    true => Ok(Self::Window(WindowEvent::FocusGained)),
                    false => Ok(Self::Window(WindowEvent::FocusLost)),
                },

                WinitWindowEvent::KeyboardInput { input, .. } => {
                    let KeyboardInput {
                        scancode,
                        state,
                        virtual_keycode,
                        ..
                    } = input;
                    match state {
                        ElementState::Pressed => Ok(Self::Input(InputEvent::Keyboard(
                            KeyboardEvent::Pressed(*scancode, *virtual_keycode),
                        ))),
                        ElementState::Released => Ok(Self::Input(InputEvent::Keyboard(
                            KeyboardEvent::Released(*scancode, *virtual_keycode),
                        ))),
                    }
                }
                WinitWindowEvent::ReceivedCharacter(c) => Ok(Self::Input(InputEvent::Keyboard(
                    KeyboardEvent::ReceivedCharacter(*c),
                ))),
                WinitWindowEvent::ModifiersChanged(state) => Ok(Self::Input(InputEvent::Keyboard(
                    KeyboardEvent::ModifiersChanged(*state),
                ))),

                WinitWindowEvent::MouseInput { state, button, .. } => match state {
                    ElementState::Pressed => {
                        Ok(Self::Input(InputEvent::Mouse(MouseEvent::Pressed(*button))))
                    }
                    ElementState::Released => Ok(Self::Input(InputEvent::Mouse(
                        MouseEvent::Released(*button),
                    ))),
                },
                WinitWindowEvent::CursorMoved { position, .. } => {
                    Ok(Self::Input(InputEvent::Mouse(MouseEvent::Moved(*position))))
                }
                WinitWindowEvent::MouseWheel { delta, .. } => Ok(Self::Input(InputEvent::Mouse(
                    MouseEvent::MouseWheel(*delta),
                ))),
                WinitWindowEvent::CursorEntered { .. } => {
                    Ok(Self::Input(InputEvent::Mouse(MouseEvent::CursorEntered)))
                }
                WinitWindowEvent::CursorLeft { .. } => {
                    Ok(Self::Input(InputEvent::Mouse(MouseEvent::CursorLeft)))
                }
                _ => Err(()),
            },
            WinitEvent::Suspended => Ok(Self::Window(WindowEvent::Suspended)),
            WinitEvent::Resumed => Ok(Self::Window(WindowEvent::Resumed)),
            _ => Err(()),
        }
    }
}
