use crate::{error::RendererResult as Result, logger::prelude::*};
use imgui::DrawData;
use imgui_luminance_renderer::Renderer as ImGuiRenderer;
use luminance_derive::{Semantics, UniformInterface, Vertex};
use luminance_front::{
    context::GraphicsContext,
    framebuffer::Framebuffer,
    pipeline::{Pipeline, PipelineState},
    render_state::RenderState,
    shader::{Program, Uniform},
    shading_gate::ShadingGate,
    tess::{Mode, Tess, TessError},
    texture::Dim2,
};
use luminance_glutin::GlutinSurface;
// use oxide_color::Color;

use crate::color::RgbaFloat;

const VERT: &str = include_str!("shaders/simple.vert");
const FRAG: &str = include_str!("shaders/simple.frag");

/// ```glsl
/// in vec2 pos;
/// in vec3 col;
/// ```
#[derive(Debug, Clone, Copy, Eq, PartialEq, Semantics)]
pub enum VertexSemantics {
    #[sem(name = "pos", repr = "[f32; 2]", wrapper = "VertexPos")]
    Position,
    #[sem(name = "col", repr = "[u8; 3]", wrapper = "VertexCol")]
    Color,
}

#[derive(Debug, Clone, Copy, PartialEq, Vertex)]
#[repr(C)]
#[vertex(sem = "VertexSemantics")]
pub struct Vertex {
    pos: VertexPos,
    #[vertex(normalized = "true")]
    col: VertexCol,
}

#[derive(Debug, UniformInterface)]
pub struct ShaderInterface {
    #[uniform(name = "t")]
    pub time: Uniform<f32>,
    pub triangle_pos: Uniform<[f32; 2]>,
}

pub struct Renderer {
    pub should_swap_buffers: bool,
    pub should_recreate_backbuffer: bool,
    pub surface: GlutinSurface,
    pub backbuffer: Framebuffer<Dim2, (), ()>,
    imgui_renderer: ImGuiRenderer,

    program: Program<VertexSemantics, (), ShaderInterface>,
}

impl Renderer {
    pub fn new(mut surface: GlutinSurface, imgui_renderer: ImGuiRenderer) -> Result<Self> {
        let backbuffer = surface.back_buffer()?;

        let program = surface
            .new_shader_program()
            .from_strings(VERT, None, None, FRAG)?
            .ignore_warnings();

        Ok(Self {
            should_swap_buffers: false,
            should_recreate_backbuffer: false,
            surface,
            backbuffer,
            imgui_renderer,

            program,
        })
    }

    pub fn swap_buffers(&mut self) {
        self.should_swap_buffers = false;
        self.surface.swap_buffers();
    }

    pub fn recreate_backbuffer(&mut self) -> Result<()> {
        self.should_recreate_backbuffer = false;
        self.backbuffer = self.surface.back_buffer()?;
        Ok(())
    }

    pub fn buffer_stuff(&mut self) -> Result<()> {
        if self.should_swap_buffers {
            self.swap_buffers();
        }
        if self.should_recreate_backbuffer {
            self.recreate_backbuffer()
        } else {
            Ok(())
        }
    }

    pub fn clear<C: Into<RgbaFloat>>(&mut self, color: C) {
        if self
            .surface
            .new_pipeline_gate()
            .pipeline(
                &self.backbuffer,
                &PipelineState::new().set_clear_color(color.into().into()),
                |_, _| Ok(()),
            )
            .assume()
            .is_ok()
        {
            self.should_swap_buffers = true;
        } else {
            self.should_swap_buffers = false;
            error!("Failed to clear screen");
        }
    }

    pub fn build_direct_tess(
        &mut self,
        vertices: &[Vertex],
    ) -> Result<Tess<Vertex, ()>, TessError> {
        self.surface
            .new_tess()
            .set_vertices(&vertices[..])
            .set_mode(Mode::Triangle)
            .build()
    }

    pub fn render_tess(&mut self, tess: &Tess<Vertex, ()>) {
        let program = &mut self.program;
        let backbuffer = &self.backbuffer;
        if self
            .surface
            .new_pipeline_gate()
            .pipeline(
                backbuffer,
                &PipelineState::default().enable_clear_color(false),
                |_, mut shd_gate| {
                    shd_gate.shade(program, |_, _, mut rdr_gate| {
                        rdr_gate.render(&RenderState::default(), |mut tess_gate| {
                            tess_gate.render(tess)
                        })
                    })
                },
            )
            .assume()
            .is_ok()
        {
            self.should_swap_buffers = true;
        } else {
            self.should_swap_buffers = false;
            error!("Failed to render tess");
        }
    }

    pub fn render_tesses(&mut self, tesses: &Vec<Tess<Vertex, ()>>) {
        let program = &mut self.program;
        let backbuffer = &self.backbuffer;
        if self
            .surface
            .new_pipeline_gate()
            .pipeline(
                backbuffer,
                &PipelineState::default().enable_clear_color(false),
                |_, mut shd_gate| {
                    shd_gate.shade(program, |_, _, mut rdr_gate| {
                        rdr_gate.render(&RenderState::default(), |mut tess_gate| {
                            for tess in tesses {
                                tess_gate.render(tess)?;
                            }
                            Ok(())
                        })
                    })
                },
            )
            .assume()
            .is_ok()
        {
            self.should_swap_buffers = true;
        } else {
            self.should_swap_buffers = false;
            error!("Failed to render tesses");
        }
    }

    pub fn render_custom<F>(&mut self, f: F)
    where
        F: for<'b> FnOnce(
            &mut Program<VertexSemantics, (), ShaderInterface>,
            Pipeline<'b>,
            ShadingGate<'b>,
        ) -> Result<(), luminance::pipeline::PipelineError>,
    {
        let program = &mut self.program;
        let backbuffer = &self.backbuffer;
        if self
            .surface
            .new_pipeline_gate()
            .pipeline(
                backbuffer,
                &PipelineState::default().enable_clear_color(false),
                |pipeline, shd_gate| f(program, pipeline, shd_gate),
            )
            .assume()
            .is_ok()
        {
            self.should_swap_buffers = true;
        } else {
            self.should_swap_buffers = false;
            error!("Failed to render custom");
        }
    }

    pub fn render_imgui(&mut self, draw_data: &DrawData) {
        let imgui_renderer = &mut self.imgui_renderer;
        imgui_renderer.prepare(&mut self.surface, draw_data);
        if self
            .surface
            .new_pipeline_gate()
            .pipeline(
                &self.backbuffer,
                &PipelineState::new().enable_clear_color(false),
                |pipeline, mut shd_gate| imgui_renderer.render(&pipeline, &mut shd_gate, draw_data),
            )
            // .assume()
            .is_ok()
        {
            self.should_swap_buffers = true;
        } else {
            self.should_swap_buffers = false;
            error!("Failed to render imgui");
        }
    }
}
