//in vec3 frag_col;
//
//out vec4 frag;
//
//void main() {
//    frag = vec4(frag_col, 1.0);
//}

in vec3 frag_col;

out vec4 frag;

uniform float t;

void main() {
    frag = vec4(frag_col * vec3(pow(cos(t), 2.), pow(sin(t), 2.), cos(t * .5)), 1.);
    frag = pow(frag, vec4(1./2.2));
}
