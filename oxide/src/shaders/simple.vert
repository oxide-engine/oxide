//in vec2 pos;
//in vec3 col;
//
//out vec3 frag_col;
//
//void main() {
//    frag_col = col;
//    gl_Position = vec4(pos, 0.0, 1.0);
//}

in vec2 pos;
in vec3 col;

out vec3 frag_col;

uniform vec2 triangle_pos;

void main() {
    frag_col = col;
    gl_Position = vec4(pos + triangle_pos, 0.0, 1.0);
}
