use crate::color::{Rgba255, RgbaFloat};
use imgui::im_str;
use snafu::Snafu;
use std::{
    collections::vec_deque::VecDeque,
    io::Write,
    ops::DerefMut,
    sync::{Arc, RwLock},
};

pub mod prelude {
    pub use log::{debug, error, info, trace, warn};
}

#[derive(Debug, Copy, Clone)]
pub enum LogColorConfigColorScheme {
    Gruvbox,
    Solorized,
}

#[derive(Debug, Copy, Clone)]
pub struct LogColorConfig {
    pub trace: RgbaFloat,
    pub debug: RgbaFloat,
    pub info: RgbaFloat,
    pub warn: RgbaFloat,
    pub error: RgbaFloat,
}

impl LogColorConfig {
    pub fn from_color_scheme(scheme: LogColorConfigColorScheme) -> Self {
        match scheme {
            LogColorConfigColorScheme::Gruvbox => Self::gruvbox(),
            LogColorConfigColorScheme::Solorized => Self::solorized(),
        }
    }

    pub fn gruvbox() -> Self {
        Self {
            trace: Rgba255::from((152, 151, 26)).into(),
            debug: Rgba255::from((177, 98, 134)).into(),
            info: Rgba255::from((69, 133, 136)).into(),
            warn: Rgba255::from((214, 93, 14)).into(),
            error: Rgba255::from((204, 36, 29)).into(),
        }
    }

    pub fn solorized() -> Self {
        Self {
            trace: Rgba255::from((133, 153, 0)).into(),
            debug: Rgba255::from((211, 54, 130)).into(),
            info: Rgba255::from((38, 139, 210)).into(),
            warn: Rgba255::from((203, 75, 22)).into(),
            error: Rgba255::from((220, 50, 47)).into(),
        }
    }
}

#[derive(Debug, Clone)]
struct LogWindow {
    pre_text_buffer: String,
    text_buffer: VecDeque<String>,
    filter: String,

    auto_scroll: bool,
    match_case: bool,

    color_config: LogColorConfig,
    color_config_none: bool,
    color_config_gruvbox: bool,
    color_config_solarized: bool,
}

// NOTE: Running any logging in here *WILL* cause a panic, relating to a rwlock, at least in unix platforms
/// InGui Window that displays rust `log` messages
impl LogWindow {
    fn new() -> Self {
        Self {
            pre_text_buffer: String::new(),
            text_buffer: VecDeque::new(),
            filter: String::new(),

            auto_scroll: true,
            match_case: false,

            color_config: LogColorConfig::gruvbox(),
            color_config_none: false,
            color_config_gruvbox: true,
            color_config_solarized: false,
        }
    }

    fn clear(&mut self) {
        self.text_buffer.clear();
        self.filter.clear();
    }

    fn flush_log(&mut self) {
        let log = &self.pre_text_buffer.clone();
        self.text_buffer.push_back(log.into());
        self.pre_text_buffer.clear();
    }

    fn change_color_scheme(&mut self, scheme: LogColorConfigColorScheme) {
        self.color_config = LogColorConfig::from_color_scheme(scheme);
    }

    fn draw(&mut self, ui: &imgui::Ui, opened: &mut bool) {
        imgui::Window::new(im_str!("Log"))
            .opened(opened)
            .position([40.0, 400.0], imgui::Condition::Once)
            .size([600.0, 300.0], imgui::Condition::Once)
            .menu_bar(true)
            .build(&ui, || {
                ui.menu_bar(|| {
                    ui.menu(im_str!("Colors"), true, || {
                        if imgui::MenuItem::new(im_str!("None"))
                            .selected(self.color_config_none)
                            .build(&ui)
                        {
                            self.color_config_none = true;
                            self.color_config_gruvbox = false;
                            self.color_config_solarized = false;
                        } else if imgui::MenuItem::new(im_str!("Gruvbox"))
                            .selected(self.color_config_gruvbox)
                            .build(&ui)
                        {
                            self.change_color_scheme(LogColorConfigColorScheme::Gruvbox);
                            self.color_config_none = false;
                            self.color_config_gruvbox = true;
                            self.color_config_solarized = false;
                        } else if imgui::MenuItem::new(im_str!("Solarized"))
                            .selected(self.color_config_solarized)
                            .build(&ui)
                        {
                            self.change_color_scheme(LogColorConfigColorScheme::Solorized);
                            self.color_config_none = false;
                            self.color_config_gruvbox = false;
                            self.color_config_solarized = true;
                        }
                    });
                });

                let clear = ui.button(im_str!("Clear"), [80.0, 20.0]);
                ui.same_line(0.0);
                ui.checkbox(im_str!("Auto-scroll"), &mut self.auto_scroll);
                ui.same_line(0.0);
                ui.checkbox(im_str!("Match case"), &mut self.match_case);

                let mut filter = imgui::ImString::new(&self.filter);
                ui.input_text(im_str!("Filter"), &mut filter).build();
                self.filter = filter.to_string();

                if clear {
                    self.clear();
                }

                imgui::ChildWindow::new("scrolling")
                    .flags(imgui::WindowFlags::HORIZONTAL_SCROLLBAR)
                    .build(&ui, || {
                        let style =
                            ui.push_style_var(imgui::StyleVar::ItemInnerSpacing([1.0, 1.0]));

                        // Very simple filter 'algorithm'.
                        // Probably a better way of doing this but it works exactly how I want it, so it stays.
                        // How it works:
                        //  Each line is stored as a `String` in a `Vec`,
                        //  when filtering we iterate over the `Vec` and run `contains`
                        //  on the line/string, and if the string contains the filter
                        //  we print it, otherwise we just print the whole thing.
                        //  With a match case option thrown in.
                        if self.filter.is_empty() {
                            for line in &self.text_buffer {
                                self.print_line(line, &ui);
                            }
                        } else {
                            if self.match_case {
                                for line in &self.text_buffer {
                                    if line.contains(&self.filter) {
                                        self.print_line(line, &ui);
                                    }
                                }
                            } else {
                                for line in &self.text_buffer {
                                    if line.to_lowercase().contains(&self.filter.to_lowercase()) {
                                        self.print_line(line, &ui);
                                    }
                                }
                            }
                        }

                        style.pop(&ui);

                        if self.auto_scroll && (ui.scroll_y() >= ui.scroll_max_y()) {
                            ui.set_scroll_here_y_with_ratio(1.0);
                            while self.text_buffer.len() > 250 {
                                self.text_buffer.pop_front();
                            }
                        }
                    });
            });
    }

    fn print_line(&self, line: &String, ui: &imgui::Ui) {
        if self.color_config_none {
            ui.text(line);
        } else if line.contains("[TRACE]") {
            ui.text_colored(self.color_config.trace.into(), line);
        } else if line.contains("[DEBUG]") {
            ui.text_colored(self.color_config.debug.into(), line);
        } else if line.contains("[INFO]") {
            ui.text_colored(self.color_config.info.into(), line);
        } else if line.contains("[WARN]") {
            ui.text_colored(self.color_config.warn.into(), line);
        } else if line.contains("[ERROR]") {
            ui.text_colored(self.color_config.error.into(), line);
        } else {
            ui.text(line);
        }
    }
}

// We need to impl io::Write for any appender
impl std::io::Write for LogWindow {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let string = String::from_utf8_lossy(buf);
        self.pre_text_buffer.push_str(&string.clone());
        Ok(string.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        self.flush_log();
        Ok(())
    }
}

// We need to impl Write for any appender, io::Write is required for this
impl log4rs::encode::Write for LogWindow {}

/// A builder for a `LogWindowAppender`
#[derive(Debug)]
pub struct LogWindowAppenderBuilder {
    encoder: Option<Box<dyn log4rs::encode::Encode>>,
}

impl LogWindowAppenderBuilder {
    pub fn new() -> Self {
        Self { encoder: None }
    }

    pub fn encoder(mut self, encoder: Box<dyn log4rs::encode::Encode>) -> Self {
        self.encoder = Some(encoder);
        self
    }

    pub fn build(self) -> LogWindowAppender {
        LogWindowAppender {
            log_window: Arc::new(RwLock::new(LogWindow::new())),
            encoder: Arc::new(RwLock::new(
                self.encoder
                    .unwrap_or(Box::new(log4rs::encode::pattern::PatternEncoder::default())),
            )),
        }
    }
}

/// An appender for LogWindow, also forwards the draw function
#[derive(Debug, Clone)]
pub struct LogWindowAppender {
    log_window: Arc<RwLock<LogWindow>>,
    encoder: Arc<RwLock<Box<dyn log4rs::encode::Encode>>>,
}

impl LogWindowAppender {
    pub fn builder() -> LogWindowAppenderBuilder {
        LogWindowAppenderBuilder::new()
    }

    // pub fn draw(&self, ui: &imgui::Ui, opened: &mut bool) {
    //     let log_window_lock = Arc::clone(&self.log_window);
    //     let mut log_window = log_window_lock.write().unwrap();
    //     log_window.draw(&ui, opened);
    // }
}

#[derive(Debug, Clone, Snafu)]
enum LogWindowAppenderError {
    #[snafu(display("Failed to acquire write lock on: {}", what))]
    FailedToAcquireWriteLock { what: String },
    #[snafu(display("Failed to acquire read lock on: {}", what))]
    FailedToAcquireReadLock { what: String },
}

impl log4rs::append::Append for LogWindowAppender {
    fn append(&self, record: &log::Record) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        let encoder_lock = Arc::clone(&self.encoder);
        let encoder = match encoder_lock.read() {
            Ok(e) => e,
            Err(_) => {
                return Err(Box::new(LogWindowAppenderError::FailedToAcquireReadLock {
                    what: "encoder".into(),
                }))
            }
        };
        let log_window_lock = Arc::clone(&self.log_window);
        let mut log_window = match log_window_lock.write() {
            Ok(l) => l,
            Err(_) => {
                return Err(Box::new(LogWindowAppenderError::FailedToAcquireReadLock {
                    what: "log_window".into(),
                }))
            }
        };
        encoder.encode(log_window.deref_mut(), record)?;
        log_window.flush()?;
        Ok(())
    }

    fn flush(&self) {}
}

impl crate::engine::layers::imgui::ImGuiWindow for LogWindowAppender {
    fn draw(&mut self, ui: &imgui::Ui, state: &mut crate::engine::layers::imgui::ImGuiState) {
        let log_window_lock = Arc::clone(&self.log_window);
        let mut log_window = log_window_lock.write().unwrap();
        if state.menu.file.logger {
            log_window.draw(&ui, &mut state.menu.file.logger);
        }
    }
}
