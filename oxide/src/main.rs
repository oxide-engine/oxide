pub mod color;
pub mod engine;
pub mod error;
pub mod event;
pub mod input;
pub mod logger;
pub mod render;

pub trait Invert {
    fn invert(&mut self);
}

impl Invert for bool {
    fn invert(&mut self) {
        *self = !*self;
    }
}

fn main() {
    let oxide = engine::oxide::Oxide::builder()
        .with_logger_level_filter(log::LevelFilter::Trace)
        .build()
        .unwrap();

    oxide.run();
}
