use super::{
    layers,
    time::{Time, ONE_60TH_OF_A_SECOND, POLL_TIME},
    OxideBuilder,
};
use crate::{
    logger::{prelude::*, LogWindowAppender},
    render::Renderer,
};
use glutin::{
    event::{Event, VirtualKeyCode},
    event_loop::{ControlFlow, EventLoop},
};
use imgui_winit_support::WinitPlatform as GlutinPlatform;
use std::{cell::RefCell, convert::TryFrom, rc::Rc};

pub struct Oxide {
    pub(super) event_loop: Option<EventLoop<()>>,
    pub(super) imgui: Option<imgui::Context>,
    pub(super) glutin_platform: Option<GlutinPlatform>,
    pub(super) renderer: Option<Renderer>,

    pub(super) log_level_filter: log::LevelFilter,
}

impl Oxide {
    pub fn builder() -> OxideBuilder {
        OxideBuilder::new()
    }

    pub fn run(mut self) {
        // `take` our objects from self so they can be moved into the event loop closure
        let event_loop: EventLoop<_> = self.event_loop.take().unwrap();
        let imgui: imgui::Context = self.imgui.take().unwrap();
        let glutin_platform: GlutinPlatform = self.glutin_platform.take().unwrap();
        let renderer: Renderer = self.renderer.take().unwrap();

        // All the code is in a different function,
        // since rust-fmt doesn't want to format this method.
        // Also one less indent.
        run(
            event_loop,
            imgui,
            glutin_platform,
            renderer,
            self.log_level_filter,
        );
    }
}

fn run(
    event_loop: EventLoop<()>,
    imgui: imgui::Context,
    glutin_platform: GlutinPlatform,
    mut renderer: Renderer,
    log_level_filter: log::LevelFilter,
) {
    // Setup logging
    let log_window = {
        use log4rs::{
            append::console::ConsoleAppender,
            config::{Appender, Config, Root},
            encode::pattern::PatternEncoder,
        };

        let stdout = {
            if cfg!(feature = "colored-console-logging") {
                ConsoleAppender::builder()
                    .encoder(Box::new(PatternEncoder::new(
                        "[{d(%Y-%m-%d %H:%M:%S)}] [{M}] [{f}:{L}] [{h({l})}] {m}{n}",
                    )))
                    .build()
            } else {
                ConsoleAppender::builder()
                    .encoder(Box::new(PatternEncoder::new(
                        "[{d(%Y-%m-%d %H:%M:%S)}] [{M}] [{f}:{L}] [{l}] {m}{n}",
                    )))
                    .build()
            }
        };

        let log_window = LogWindowAppender::builder()
            .encoder(Box::new(PatternEncoder::new(
                "[{d(%Y-%m-%d %H:%M:%S)}] [{M}] [{l}] {m}{n}",
            )))
            .build();

        let config = Config::builder()
            .appender(Appender::builder().build("stdout", Box::new(stdout)))
            .appender(Appender::builder().build("log_window", Box::new(log_window.clone())))
            .build(
                Root::builder()
                    .appender("stdout")
                    .appender("log_window")
                    .build(log_level_filter),
            )
            .unwrap();

        log4rs::init_config(config).unwrap();
        log_window
    };

    // Time
    let mut time = Time::new(ONE_60TH_OF_A_SECOND, ONE_60TH_OF_A_SECOND);

    // Clear color
    let mut clear_color = [0.0, 0.0, 0.0];
    // let mut clear_color = (0.0, 0.0, 0.0);

    // Imgui layer
    let imgui_layer = Rc::new(RefCell::new(layers::imgui::ImGui::new(
        imgui,
        glutin_platform,
    )));

    // Main menu bar
    imgui_layer.borrow_mut().push_window(
        Rc::new(RefCell::new(
            layers::imgui::main_menu_bar::MainMenuBar::default(),
        )),
        true,
        0,
    );
    // Metrics window
    imgui_layer.borrow_mut().push_window(
        Rc::new(RefCell::new(layers::imgui::metrics::Metrics::default())),
        true,
        1,
    );
    // Built in imgui windows
    imgui_layer.borrow_mut().push_window(
        Rc::new(RefCell::new(layers::imgui::builtin::Builtin::default())),
        true,
        2,
    );
    // Hello World window
    imgui_layer.borrow_mut().push_window(
        Rc::new(RefCell::new(
            layers::imgui::hello_world::HelloWorld::default(),
        )),
        true,
        3,
    );
    // Logger window
    imgui_layer
        .borrow_mut()
        .push_window(Rc::new(RefCell::new(log_window)), true, 4);

    // Layer machine
    let mut layer_machine = layers::LayerMachine::new();
    layer_machine.push_overlay(imgui_layer.clone(), true, 0);

    // Test layer
    layer_machine.push_layer(
        Rc::new(RefCell::new(layers::test_layer::TestLayer::new(
            &mut renderer,
        ))),
        true,
        1,
    );

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::WaitUntil(std::time::Instant::now() + POLL_TIME);
        // *control_flow = ControlFlow::Wait;
        // *control_flow = ControlFlow::Poll;

        match event {
            Event::NewEvents(_cause) => {
                // Code before we process events

                time.app_time.update_fps();
                time.calculate_steps();
                time.app_time.update_delta();
            }
            Event::MainEventsCleared => {
                // Application logic

                // Update
                if time.should_fixed_update.should_step {
                    time.should_fixed_update.should_step = false;
                    layer_machine.fixed_update(&time);
                }
                layer_machine.update(&time);

                {
                    let mut imgui_layer = imgui_layer.borrow_mut();
                    let state = &mut imgui_layer.state.borrow_mut();

                    // state.clear_color = clear_color;
                    clear_color = state.clear_color;

                    if state.menu.file.exit {
                        *control_flow = ControlFlow::Exit;
                    }
                }

                // Draw
                if time.should_draw.should_step {
                    time.should_draw.should_step = false;
                    renderer.surface.ctx.window().request_redraw();
                }
            }
            Event::RedrawRequested(_) => {
                // Rendering code

                time.draw_time.update_fps();

                renderer.clear(clear_color);

                // Render
                layer_machine.render(&mut renderer);

                // Buffer stuff
                if let Some(err) = renderer.buffer_stuff().err() {
                    error!(
                        "Failed to recreate `backbuffer`, attempting to continue:\
                         \n\ncaused by:\n\t{}",
                        err
                    );
                }

                // Draw delta
                time.draw_time.update_delta();
            }
            event => {
                // The rest of event handling

                layer_machine.window_event_winit(&event, renderer.surface.ctx.window());

                if let Ok(event) = TryFrom::try_from(&event) {
                    use crate::event::{Event, InputEvent, KeyboardEvent, WindowEvent};

                    layer_machine.window_event(&event, renderer.surface.ctx.window());

                    match event {
                        Event::Window(event) => match event {
                            WindowEvent::Resized(_) | WindowEvent::ScaleFactorChanged(_) => {
                                renderer.should_recreate_backbuffer = true
                            }
                            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                            _ => (),
                        },
                        Event::Input(input) => {
                            // debug!("{:#?}", input);
                            match input {
                                InputEvent::Keyboard(input) => match input {
                                    KeyboardEvent::Pressed(_, Some(key)) => match key {
                                        VirtualKeyCode::F12 => *control_flow = ControlFlow::Exit,
                                        _ => (),
                                    },
                                    _ => (),
                                },
                                _ => (),
                            }
                        }
                    }
                };
            }
        }
    });
}
