use crate::{error::OxideBuilderResult as Result, render::Renderer};
use glutin::{
    dpi::{PhysicalSize, Size},
    event_loop::EventLoop,
};
use imgui_luminance_renderer::Renderer as ImGuiRenderer;
use imgui_winit_support::WinitPlatform as GlutinPlatform;
use luminance_glutin::GlutinSurface;
use serde::{Deserialize, Serialize};

pub mod layers;
pub mod oxide;
pub mod time;

use oxide::Oxide;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub name: String,
    pub window_config: WindowConfig,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            name: "Oxide".to_string(),
            window_config: WindowConfig::default(),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WindowConfig {
    pub title: String,
    pub size: [u32; 2],
}

impl Default for WindowConfig {
    fn default() -> Self {
        Self {
            title: "Oxide".to_string(),
            size: [800, 600],
        }
    }
}

#[derive(Clone)]
pub struct OxideBuilder {
    config: Config,
    log_level_filter: log::LevelFilter,
}

impl OxideBuilder {
    pub fn new() -> Self {
        Self {
            config: Config::default(),
            log_level_filter: log::LevelFilter::Info,
        }
    }

    pub fn with_config(mut self, config: Config) -> Self {
        self.config = config;
        self
    }

    pub fn with_ron_config(mut self, config: &str) -> Result<Self> {
        self.config = ron::from_str(config)?;
        Ok(self)
    }

    pub fn with_logger_level_filter(mut self, level: log::LevelFilter) -> Self {
        self.log_level_filter = level;
        self
    }

    pub fn build(self) -> Result<Oxide> {
        let log_level_filter = self.log_level_filter;

        // Create glutin surface
        let (mut surface, event_loop): (GlutinSurface, EventLoop<()>) =
            GlutinSurface::new_gl33_from_builders(
                |_event_loop, window_builder| {
                    let config = self.config.window_config;
                    window_builder
                        .with_title(config.title)
                        .with_inner_size(Size::Physical(PhysicalSize::new(
                            config.size[0],
                            config.size[1],
                        )))
                },
                |_event_loop, ctx_builder| ctx_builder.with_double_buffer(Some(true)),
            )?;

        // Create imgui Context
        let mut imgui = imgui::Context::create();
        imgui.set_ini_filename(None);

        // Imgui window initialization
        let mut glutin_platform = GlutinPlatform::init(&mut imgui);
        glutin_platform.attach_window(
            imgui.io_mut(),
            &surface.ctx.window(),
            imgui_winit_support::HiDpiMode::Default,
        );

        // Create renderers
        let imgui_renderer = ImGuiRenderer::new(&mut surface, &mut imgui);
        let renderer = Renderer::new(surface, imgui_renderer)?;

        Ok(Oxide {
            event_loop: Some(event_loop),
            imgui: Some(imgui),
            glutin_platform: Some(glutin_platform),
            renderer: Some(renderer),

            log_level_filter,
        })
    }
}
