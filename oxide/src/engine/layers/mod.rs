use crate::{engine::time::Time, event::Event, logger::prelude::*, render::Renderer};
use std::{cell::RefCell, rc::Rc};

pub mod imgui;
pub mod test_layer;

pub mod prelude {
    pub use super::{Handled, Layer};
    pub use crate::{
        engine::time::Time,
        event::{Event, InputEvent, KeyboardEvent, MouseEvent, VirtualKeyCode, WindowEvent},
        render::Renderer,
    };
    pub use glutin::window::Window;
}

pub trait Layer {
    /// Run when pushed to the stack
    #[allow(unused_variables)]
    fn on_attach(&mut self) {}

    /// Run when removed from the stack
    fn on_detach(&mut self) {}

    /// Run every frame as fast as possible
    #[allow(unused_variables)]
    fn on_update(&mut self, time: &Time) {}

    /// Run every 60th of a second
    #[allow(unused_variables)]
    fn on_fixed_update(&mut self, time: &Time) {}

    /// Run on evey draw call
    #[allow(unused_variables)]
    fn on_draw(&mut self, renderer: &mut Renderer) {}

    /// Run on each window event,
    /// return [`Handled::Yes`] to stop propagating the event,
    /// and [`Handled::No`] to keep propagating the event.
    #[allow(unused_variables)]
    fn on_window_event(&mut self, event: &Event, window: &glutin::window::Window) -> Handled {
        Handled::No
    }

    /// Run on each unconverted-winit window event,
    /// return [`Handled::Yes`] to stop propagating the event,
    /// and [`Handled::No`] to keep propagating the event.
    #[allow(unused_variables)]
    fn on_window_event_winit(
        &mut self,
        event: &glutin::event::Event<()>,
        window: &glutin::window::Window,
    ) -> Handled {
        Handled::No
    }
}

pub enum Handled {
    Yes,
    No,
}

pub struct LayerBox {
    layer: Rc<RefCell<dyn Layer>>,
    enabled: bool,
    id: usize,
}

impl LayerBox {
    fn new(layer: Rc<RefCell<dyn Layer>>, enabled: bool, id: usize) -> Self {
        Self { layer, enabled, id }
    }
}

pub struct LayerMachine {
    layer_stack: Vec<LayerBox>,
    layer_insert_index: usize,
}

impl LayerMachine {
    pub fn new() -> Self {
        Self {
            layer_stack: Vec::new(),
            layer_insert_index: 0,
        }
    }

    /// Run `on_update` on all the enabled layers
    pub fn update(&mut self, time: &Time) {
        for layer in &mut self.layer_stack.iter_mut() {
            if layer.enabled {
                layer.layer.borrow_mut().on_update(time);
            }
        }
    }

    /// Run `on_fixed_update` on all the enabled layers
    pub fn fixed_update(&mut self, time: &Time) {
        for layer in &mut self.layer_stack.iter_mut() {
            if layer.enabled {
                layer.layer.borrow_mut().on_fixed_update(time);
            }
        }
    }

    /// Run `on_draw` on all the enabled layers
    pub fn render(&mut self, renderer: &mut Renderer) {
        for layer in &mut self.layer_stack.iter_mut() {
            if layer.enabled {
                layer.layer.borrow_mut().on_draw(renderer);
            }
        }
    }

    /// Run `on_event` on all the enabled layers
    pub fn window_event(&mut self, event: &Event, window: &glutin::window::Window) {
        for layer in &mut self.layer_stack.iter_mut().rev() {
            if layer.enabled {
                match layer.layer.borrow_mut().on_window_event(event, window) {
                    Handled::No => continue,
                    Handled::Yes => break,
                }
            }
        }
    }

    /// Run `on_event_winit` on all the enabled layers
    pub fn window_event_winit(
        &mut self,
        event: &glutin::event::Event<()>,
        window: &glutin::window::Window,
    ) {
        for layer in &mut self.layer_stack.iter_mut().rev() {
            if layer.enabled {
                match layer
                    .layer
                    .borrow_mut()
                    .on_window_event_winit(event, window)
                {
                    Handled::No => continue,
                    Handled::Yes => break,
                }
            }
        }
    }

    /// Go though all layers and try to find the given id
    fn get_any_layer_index_from_id(&self, id: usize) -> Option<usize> {
        self.layer_stack.iter().position(|l| l.id == id)
    }
}

/// # Layers
impl LayerMachine {
    /// Push a layer to the layer stack
    pub fn push_layer(&mut self, layer: Rc<RefCell<dyn Layer>>, enabled: bool, id: usize) {
        if let Some(index) = self.get_any_layer_index_from_id(id) {
            error!(
                "Unable to push Layer with id: `{}`, id already exists at index: `{}`",
                id, index
            );
        } else {
            let layer_box = LayerBox::new(layer, enabled, id);
            layer_box.layer.borrow_mut().on_attach();
            self.layer_stack.insert(self.layer_insert_index, layer_box);
            self.layer_insert_index += 1;
        }
    }

    /// Remove a layer with the given id
    pub fn remove_layer(&mut self, id: usize) {
        if let Some(index) = self.get_layer_index_from_id(id) {
            self.layer_stack
                .get_mut(index)
                .unwrap()
                .layer
                .borrow_mut()
                .on_detach();
            self.layer_stack.remove(index);
            self.layer_insert_index -= 1;
        } else {
            error!("Unable to remove Layer with id: `{}`, Id not found", id);
        }
    }

    /// Set the enabled state of a layer
    pub fn enable_layer(&mut self, id: usize, enable: bool) {
        if let Some(layer) = self.get_layer_box_from_id(id) {
            layer.enabled = enable;
        } else {
            error!(
                "Unable to change Layer enabled state with id: `{}`, Id not found",
                id
            );
        }
    }

    /// Get a layers index from an id
    fn get_layer_index_from_id(&self, id: usize) -> Option<usize> {
        self.layer_stack[0..self.layer_insert_index]
            .iter()
            .position(|l| l.id == id)
    }

    /// Get a layer_box from an id
    fn get_layer_box_from_id(&mut self, id: usize) -> Option<&mut LayerBox> {
        self.layer_stack[0..self.layer_insert_index]
            .iter_mut()
            .find(|l| l.id == id)
    }
}

/// # Overlays
impl LayerMachine {
    // Push an overlay to the overlay stack
    pub fn push_overlay(&mut self, overlay: Rc<RefCell<dyn Layer>>, enabled: bool, id: usize) {
        if let Some(index) = self.get_any_layer_index_from_id(id) {
            error!(
                "Unable to push Overlay with id: `{}`, id already exists at index: `{}`",
                id, index
            );
        } else {
            let layer_box = LayerBox::new(overlay, enabled, id);
            layer_box.layer.borrow_mut().on_attach();
            self.layer_stack.push(layer_box);
        }
    }

    /// Remove an overlay with the given id
    pub fn remove_overlay(&mut self, id: usize) {
        if let Some(index) = self.get_overlay_index_from_id(id) {
            self.layer_stack
                .get_mut(index)
                .unwrap()
                .layer
                .borrow_mut()
                .on_detach();
            self.layer_stack.remove(index);
        } else {
            error!("Unable to remove Overlay with id: `{}`, Id not found", id);
        }
    }

    /// Set the enabled state of an overlay
    pub fn enable_overlay(&mut self, id: usize, enable: bool) {
        if let Some(overlay) = self.get_overlay_box_from_id(id) {
            overlay.enabled = enable;
        } else {
            error!(
                "Unable to change Overlay enabled state with id: `{}`, Id not found",
                id
            );
        }
    }

    /// Get an overlays index from an id
    fn get_overlay_index_from_id(&self, id: usize) -> Option<usize> {
        self.layer_stack[self.layer_insert_index..]
            .iter()
            .position(|l| l.id == id)
    }

    /// Get an overlay_box from an id
    fn get_overlay_box_from_id(&mut self, id: usize) -> Option<&mut LayerBox> {
        self.layer_stack[self.layer_insert_index..]
            .iter_mut()
            .find(|l| l.id == id)
    }
}
