use super::prelude::*;
use crate::render::{Vertex, VertexCol, VertexPos};
use luminance_front::{render_state::RenderState, tess::Tess};

#[cfg_attr(rustfmt, rustfmt_skip)]
pub const TRIANGLE: [Vertex; 3] = [
    Vertex::new(VertexPos::new([ 0.5, -0.5]), VertexCol::new([  0, 255,   0])),
    Vertex::new(VertexPos::new([ 0.0,  0.5]), VertexCol::new([  0,   0, 255])),
    Vertex::new(VertexPos::new([-0.5, -0.5]), VertexCol::new([255,   0,   0])),
];

#[cfg_attr(rustfmt, rustfmt_skip)]
pub const TRI_RED_BLUE_VERTICES: [Vertex; 6] = [
    // red
    Vertex::new(VertexPos::new([ 0.5, -0.5]), VertexCol::new([255,   0,   0])),
    Vertex::new(VertexPos::new([ 0.0,  0.5]), VertexCol::new([255,   0,   0])),
    Vertex::new(VertexPos::new([-0.5, -0.5]), VertexCol::new([255,   0,   0])),
    // blue
    Vertex::new(VertexPos::new([-0.5,  0.5]), VertexCol::new([  0,   0, 255])),
    Vertex::new(VertexPos::new([ 0.0, -0.5]), VertexCol::new([  0,   0, 255])),
    Vertex::new(VertexPos::new([ 0.5,  0.5]), VertexCol::new([  0,   0, 255])),
];

pub struct TestLayer {
    tess: Tess<Vertex, ()>,
    triangle_pos: [f32; 2],
    t: f32,
}

impl TestLayer {
    pub fn new(renderer: &mut Renderer) -> Self {
        // let tess = renderer
        //     .build_direct_tess(&TRI_RED_BLUE_VERTICES[..])
        //     .unwrap();
        let tess = renderer.build_direct_tess(&TRIANGLE[..]).unwrap();

        Self {
            tess,
            triangle_pos: [0.0, 0.0],
            t: 0.0,
        }
    }
}

impl Layer for TestLayer {
    fn on_update(&mut self, time: &Time) {
        self.t = (time.app_time.delta.as_secs() as f64
            + (time.app_time.delta.subsec_millis() as f64 * 1e-3)) as f32;
    }

    fn on_draw(&mut self, renderer: &mut Renderer) {
        renderer.render_custom(|program, _, mut shd_gate| {
            shd_gate.shade(program, |mut iface, uni, mut rdr_gate| {
                iface.set(&uni.time, self.t);
                iface.set(&uni.triangle_pos, self.triangle_pos);

                rdr_gate.render(&RenderState::default(), |mut tess_gate| {
                    tess_gate.render(&self.tess)
                })
            })
        });
    }

    fn on_window_event(&mut self, event: &Event, _window: &Window) -> Handled {
        match event {
            Event::Input(InputEvent::Keyboard(key)) => match key {
                KeyboardEvent::Pressed(_, Some(code)) => match code {
                    VirtualKeyCode::A => self.triangle_pos[0] -= 0.1,
                    VirtualKeyCode::D => self.triangle_pos[0] += 0.1,
                    VirtualKeyCode::S => self.triangle_pos[1] -= 0.1,
                    VirtualKeyCode::W => self.triangle_pos[1] += 0.1,
                    _ => (),
                },
                _ => (),
            },
            _ => (),
        }

        Handled::No
    }
}
