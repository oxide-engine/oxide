use super::prelude::*;

#[derive(Default)]
pub struct Builtin;

impl ImGuiWindow for Builtin {
    fn draw(&mut self, ui: &imgui::Ui, state: &mut ImGuiState) {
        if state.menu.help.imgui.demo {
            ui.show_demo_window(&mut state.menu.help.imgui.demo);
        }
        if state.menu.help.imgui.metrics {
            ui.show_metrics_window(&mut state.menu.help.imgui.metrics);
        }
        if state.menu.help.imgui.about {
            ui.show_about_window(&mut state.menu.help.imgui.about);
        }
    }
}
