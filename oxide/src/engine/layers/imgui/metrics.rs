use super::prelude::*;

#[derive(Default)]
pub struct Metrics;

impl ImGuiWindow for Metrics {
    fn draw(&mut self, ui: &imgui::Ui, state: &mut ImGuiState) {
        if state.menu.help.metrics {
            let mut opened = true;
            #[cfg_attr(rustfmt, rustfmt_skip)]
            imgui::Window::new(im_str!("Metrics"))
                .opened(&mut opened)
                .build(&ui, || {
                    ui.text(im_str!("App current time: {:?}",  state.time.app_time.current_time));
                    ui.text(im_str!("App previous time: {:?}", state.time.app_time.previous_time));
                    ui.text(im_str!("App delta: {:?}",         state.time.app_time.delta));
                    ui.text(im_str!("App frame count: {}",     state.time.app_time.frame_count));
                    ui.text(im_str!("App fps: {}",             state.time.app_time.fps));
                    ui.separator();
                    ui.text(im_str!("Draw current time: {:?}",  state.time.draw_time.current_time));
                    ui.text(im_str!("Draw previous time: {:?}", state.time.draw_time.previous_time));
                    ui.text(im_str!("Draw delta: {:?}",         state.time.draw_time.delta));
                    ui.text(im_str!("Draw frame count: {}",     state.time.draw_time.frame_count));
                    ui.text(im_str!("Draw fps: {}",             state.time.draw_time.fps));
                });

            if !opened {
                state.menu.help.metrics = false;
            }
        }
    }
}
