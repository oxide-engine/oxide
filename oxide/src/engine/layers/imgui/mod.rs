use super::prelude::*;
use crate::logger::prelude::*;
use defaults::Defaults;
use imgui_winit_support::WinitPlatform;
use std::{cell::RefCell, rc::Rc};

pub mod builtin;
pub mod hello_world;
pub mod log;
pub mod main_menu_bar;
pub mod metrics;

pub mod prelude {
    pub use super::{ImGuiState, ImGuiWindow};
    pub use crate::Invert;
    pub use imgui::im_str;
}

pub struct WindowBox {
    window: Rc<RefCell<dyn ImGuiWindow>>,
    enabled: bool,
    id: usize,
}

impl WindowBox {
    fn new(window: Rc<RefCell<dyn ImGuiWindow>>, enabled: bool, id: usize) -> Self {
        Self {
            window,
            enabled,
            id,
        }
    }
}

#[derive(Debug, Clone, Defaults)]
pub struct ImGuiState {
    pub menu: main_menu_bar::menu::Menu,
    pub time: Time,
    #[def = "[0.0, 0.0, 0.0]"]
    pub clear_color: [f32; 3],
}

pub struct ImGui {
    imgui: imgui::Context,
    glutin_platform: WinitPlatform,
    pub state: Rc<RefCell<ImGuiState>>,
    windows: Vec<WindowBox>,
}

impl ImGui {
    pub fn new(imgui: imgui::Context, glutin_platform: WinitPlatform) -> Self {
        Self {
            imgui,
            glutin_platform,
            state: Rc::new(RefCell::new(ImGuiState::default())),
            windows: Vec::new(),
        }
    }

    /// Add a window to the stack
    pub fn push_window(&mut self, window: Rc<RefCell<dyn ImGuiWindow>>, enabled: bool, id: usize) {
        // Make sure the id is not already in use
        if let Some((i, window_box)) = self.get_window_box_by_id(id) {
            error!(
                "Unable to push Window with id: `{}`, Window with id: `{}` already exists at index: `{}`",
                id,
                window_box.id,
                i,
            );
            return;
        }

        // Push the window
        self.windows.push(WindowBox::new(window, enabled, id));
    }

    pub fn pop_window(&mut self) {
        self.windows.pop();
    }

    /// Disable a window
    pub fn disable_window_with_id(&mut self, id: usize) {
        let window_box = match self.get_window_box_by_id(id) {
            Some(w) => w.1,
            None => {
                error!("Unable to disable Window with id: `{}`, Id not found.", id);
                return;
            }
        };
        window_box.enabled = false;
    }

    /// Enable a window
    pub fn enable_window_with_id(&mut self, id: usize) {
        let window_box = match self.get_window_box_by_id(id) {
            Some(w) => w.1,
            None => {
                error!("Unable to enable Window with id: `{}`, Id not found.", id);
                return;
            }
        };
        window_box.enabled = true;
    }

    /// Remove a window from the stack
    pub fn remove_window_with_id(&mut self, id: usize) {
        let index = match self.get_window_box_by_id(id) {
            Some(i) => i.0,
            None => {
                error!("Unable to remove Window with id: `{}`, Id not found.", id);
                return;
            }
        };
        self.windows.remove(index);
    }

    /// Get a `window_box` and its index from its id
    pub fn get_window_box_by_id(&mut self, id: usize) -> Option<(usize, &mut WindowBox)> {
        for (i, window_box) in self.windows.iter_mut().enumerate() {
            if window_box.id == id {
                return Some((i, window_box));
            }
        }
        None
    }

    /// Get a `window_box` from its index
    pub fn get_window_box_by_index(&mut self, index: usize) -> Option<&mut WindowBox> {
        self.windows.get_mut(index)
    }
}

impl Layer for ImGui {
    fn on_update(&mut self, time: &Time) {
        self.imgui.io_mut().update_delta_time(time.draw_time.delta);
        self.state.borrow_mut().time = time.clone();
    }

    fn on_draw(&mut self, renderer: &mut Renderer) {
        // Prepare frame
        if let Some(err) = self
            .glutin_platform
            .prepare_frame(self.imgui.io_mut(), renderer.surface.ctx.window())
            .err()
        {
            error!(
                "Failed to prepare imgui frame, attempting to continue:\n\ncaused by:\n\t{}",
                err
            );
        }

        let ui = self.imgui.frame();

        // Draw our windows
        let mut state = self.state.borrow_mut();
        for window in self.windows.iter_mut() {
            if window.enabled {
                window.window.borrow_mut().draw(&ui, &mut state);
            }
        }

        // Prepare render
        self.glutin_platform
            .prepare_render(&ui, renderer.surface.ctx.window());

        // Render
        renderer.render_imgui(ui.render());
    }

    fn on_window_event_winit(
        &mut self,
        event: &glutin::event::Event<()>,
        window: &glutin::window::Window,
    ) -> Handled {
        self.glutin_platform
            .handle_event(self.imgui.io_mut(), window, event);
        Handled::No
    }
}

pub trait ImGuiWindow {
    #[allow(unused_variables)]
    fn draw(&mut self, ui: &imgui::Ui, state: &mut ImGuiState) {}
}
