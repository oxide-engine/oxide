use super::prelude::*;
use crate::logger::prelude::*;

#[derive(Default)]
pub struct MainMenuBar;

pub mod menu {
    use defaults::Defaults;

    #[derive(Debug, Defaults, Clone)]
    pub struct Menu {
        #[def = "true"]
        pub show: bool,
        pub file: file::File,
        pub debug: debug::Debug,
        pub help: help::Help,
    }

    pub mod file {
        use defaults::Defaults;

        #[derive(Debug, Defaults, Clone)]
        pub struct File {
            #[def = "true"]
            pub logger: bool,
            pub exit: bool,
        }
    }

    pub mod debug {
        #[derive(Debug, Default, Clone)]
        pub struct Debug {
            // pub add_test_logs: bool,
        }
    }

    pub mod help {
        #[derive(Debug, Default, Clone)]
        pub struct Help {
            pub metrics: bool,
            pub imgui: imgui::ImGui,
        }

        pub mod imgui {
            #[derive(Debug, Default, Clone)]
            pub struct ImGui {
                pub metrics: bool,
                pub demo: bool,
                pub about: bool,
            }
        }
    }
}

impl ImGuiWindow for MainMenuBar {
    fn draw(&mut self, ui: &imgui::Ui, state: &mut ImGuiState) {
        if state.menu.show {
            ui.main_menu_bar(|| {
                // File
                ui.menu(im_str!("File"), true, || {
                    // Logging
                    if imgui::MenuItem::new(im_str!("Log window"))
                        .selected(state.menu.file.logger)
                        .build(&ui)
                    {
                        state.menu.file.logger.invert();
                    }
                    // Exit
                    else if imgui::MenuItem::new(im_str!("Exit"))
                        .shortcut(im_str!("F12"))
                        .build(&ui)
                    {
                        state.menu.file.exit = true;
                    }
                });

                // Debug
                ui.menu(im_str!("Debug"), true, || {
                    // Add logs
                    if imgui::MenuItem::new(im_str!("Add test logs")).build(&ui) {
                        // state.menu.debug.add_test_logs = true;
                        trace!("Trace!");
                        debug!("Debug!");
                        info!("Info!");
                        warn!("Warn!");
                        error!("Error!");
                    }
                });

                // Help
                ui.menu(im_str!("Help"), true, || {
                    // Metrics
                    if imgui::MenuItem::new(im_str!("Metrics"))
                        .selected(state.menu.help.metrics)
                        .build(&ui)
                    {
                        state.menu.help.metrics.invert();
                    }

                    // Imgui
                    ui.menu(im_str!("ImGui"), true, || {
                        // Metrics
                        if imgui::MenuItem::new(im_str!("Metrics"))
                            .selected(state.menu.help.imgui.metrics)
                            .build(&ui)
                        {
                            state.menu.help.imgui.metrics.invert();
                        }
                        // Demo
                        else if imgui::MenuItem::new(im_str!("Demo"))
                            .selected(state.menu.help.imgui.demo)
                            .build(&ui)
                        {
                            state.menu.help.imgui.demo.invert();
                        }
                        // About
                        else if imgui::MenuItem::new(im_str!("About ImGui"))
                            .selected(state.menu.help.imgui.about)
                            .build(&ui)
                        {
                            state.menu.help.imgui.about.invert();
                        }
                    });
                });
            });
        }
    }
}
