// use super::prelude::*;
//
// use defaults::Defaults;
// use oxide_color::Color;
//
// #[derive(Debug, Clone, Defaults)]
// #[def = "Gruvbox"]
// pub enum ColorScheme {
//     None,
//     Gruvbox,
//     Solorized,
// }
//
// #[derive(Debug, Clone)]
// pub struct ColorConfig {
//     pub trace: Color,
//     pub debug: Color,
//     pub info: Color,
//     pub warn: Color,
//     pub error: Color,
// }
//
// impl ColorConfig {
//     pub fn from_color_scheme(color_scheme: ColorScheme) -> Self {
//         match color_scheme {
//             ColorScheme::Gruvbox => Self::gruvbox(),
//             ColorScheme::Solorized => Self::solorized(),
//             None => Self::gruvbox(),
//         }
//     }
//
//     pub fn gruvbox() -> Self {
//         Self {
//             trace: Color::from_hex_rgb("98971a").unwrap(),
//             debug: Color::from_hex_rgb("b16286").unwrap(),
//             info: Color::from_hex_rgb("458588").unwrap(),
//             warn: Color::from_hex_rgb("d65d0e").unwrap(),
//             error: Color::from_hex_rgb("cc241d").unwrap(),
//         }
//     }
//
//     pub fn solorized() -> Self {
//         Self {
//             trace: Color::from_hex_rgb("859900").unwrap(),
//             debug: Color::from_hex_rgb("d33682").unwrap(),
//             info: Color::from_hex_rgb("268bd2").unwrap(),
//             warn: Color::from_hex_rgb("cb4b16").unwrap(),
//             error: Color::from_hex_rgb("dc322f").unwrap(),
//         }
//     }
// }
//
// impl Default for ColorConfig {
//     fn default() -> Self {
//         Self::from_color_scheme(Default::default())
//     }
// }
//
// struct Logger {
//     pre_text_buffer: String,
//     text_buffer: Vec<String>,
//     filter: String,
//
//     auto_scroll: bool,
//     match_case: bool,
//
//     color_scheme: ColorScheme,
//     color_config: ColorConfig,
// }
//
// impl Logger {
//     fn new() -> Self {
//         Self {
//             pre_text_buffer: String::new(),
//             text_buffer: Vec::new(),
//             filter: String::new(),
//
//             auto_scroll: true,
//             match_case: false,
//
//             color_scheme: ColorScheme::default(),
//             color_config: ColorConfig::default(),
//         }
//     }
//
//     fn clear(&mut self) {
//         self.text_buffer.clear();
//         self.filter.clear();
//     }
//
//     fn flush_log(&mut self) {
//         let log = &self.pre_text_buffer.clone();
//         self.text_buffer.push(log.into());
//         self.pre_text_buffer.clear();
//     }
//
//     fn change_color_scheme(&mut self, color_scheme: ColorScheme) {
//         self.color_config = ColorConfig::from_color_scheme(color_scheme.clone());
//     }
// }
//
// impl ImGuiWindow for Logger {
//     fn draw(&mut self, _ui: &imgui::Ui, state: &mut ImGuiState) {
//         if self.color_scheme != state.log_color_scheme {
//             self.change_color_scheme(state.log_color_scheme.clone());
//         }
//
//         let
//     }
// }
