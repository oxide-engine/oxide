use super::prelude::*;

#[derive(Default)]
pub struct HelloWorld;

impl ImGuiWindow for HelloWorld {
    fn draw(&mut self, ui: &imgui::Ui, state: &mut ImGuiState) {
        imgui::Window::new(im_str!("Hello World"))
            .size([350.0, 150.0], imgui::Condition::Appearing)
            .build(&ui, || {
                ui.text(im_str!("Hello, World!"));
                ui.separator();
                ui.checkbox(im_str!("Show demo window"), &mut state.menu.help.imgui.demo);
                ui.checkbox(im_str!("Show Logging window"), &mut state.menu.file.logger);
                imgui::ColorEdit::new(im_str!("Clear Color"), &mut state.clear_color).build(&ui);

                if ui.button(im_str!("button"), [120.0, 20.0]) {}
            });
    }
}
