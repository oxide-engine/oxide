use defaults::Defaults;
use std::time::{Duration, Instant};

// use std::sync::{Arc, RwLock};
// use crate::logger::prelude::*;

pub static ONE_SECOND: Duration = Duration::from_secs(1);
pub static ONE_60TH_OF_A_SECOND: Duration = Duration::from_micros(16670);

pub static POLL_TIME: Duration = Duration::from_micros(10);

#[derive(Default, Debug, Clone)]
pub struct Time {
    pub app_time: KeptTime,
    pub draw_time: KeptTime,
    pub should_draw: SteppedTime,
    pub should_fixed_update: SteppedTime,
}

impl Time {
    pub fn new(
        should_draw_step_interval: Duration,
        should_fixed_update_step_interval: Duration,
    ) -> Self {
        let mut x = Self::default();
        x.should_draw.step_interval = should_draw_step_interval;
        x.should_fixed_update.step_interval = should_fixed_update_step_interval;
        x
    }

    pub fn calculate_steps(&mut self) {
        self.should_fixed_update
            .calculate_step(self.app_time.current_time);
        self.should_draw.calculate_step(self.app_time.current_time);
    }
}

// #[derive(Default, Debug, Clone)]
// pub struct TimeThreaded {
//     pub app_time: Arc<RwLock<KeptTime>>,
//     pub draw_time: Arc<RwLock<KeptTime>>,
//     pub should_draw: Arc<RwLock<SteppedTime>>,
//     pub should_fixed_update: Arc<RwLock<SteppedTime>>,
// }
//
// impl TimeThreaded {
//     pub fn new(
//         should_draw_step_interval: Duration,
//         should_fixed_update_step_interval: Duration,
//     ) -> Self {
//         let mut x = Self::default();
//         x.should_draw.write().unwrap().step_interval = should_draw_step_interval;
//         x.should_fixed_update.write().unwrap().step_interval = should_fixed_update_step_interval;
//         x
//     }
//
//     pub fn increment_app_frame_count(&mut self) {
//         match self.app_time.write() {
//             Ok(a) => {
//                 should_try = false;
//                 a.frame_count += 1;
//             }
//             Err(e) => {
//                 error!("Failed to increment app frame count: {}", e);
//             }
//         }
//     }
//
//     pub fn increment_draw_frame_count(&mut self) {
//         match self.draw_time.write() {
//             Ok(d) => {
//                 should_try = false;
//                 d.frame_count += 1;
//             }
//             Err(e) => {
//                 error!("Failed to increment draw frame count: {}", e);
//             }
//         }
//     }
// }

#[derive(Debug, Clone, Defaults)]
pub struct KeptTime {
    #[def = "Instant::now()"]
    pub previous_time: Instant,
    pub delta: Duration,

    #[def = "Instant::now()"]
    pub current_time: Instant,
    #[def = "Instant::now()"]
    pub previous_time_frame_count: Instant,
    pub frame_count: u32,
    pub fps: u32,
}

impl KeptTime {
    pub fn update_fps(&mut self) {
        self.current_time = Instant::now();
        self.frame_count += 1;
        if (self.current_time - self.previous_time_frame_count) >= ONE_SECOND {
            self.fps = self.frame_count;
            self.frame_count = 0;
            self.previous_time_frame_count = self.current_time;
        }
    }

    pub fn update_delta(&mut self) -> Duration {
        self.delta = self.previous_time.elapsed();
        self.previous_time = Instant::now();
        self.delta
    }
}

#[derive(Defaults, Debug, Clone)]
pub struct SteppedTime {
    #[def = "Instant::now()"]
    pub prev_time: Instant,
    #[def = "ONE_SECOND"]
    pub step_interval: Duration,
    pub should_step: bool,
}

impl SteppedTime {
    pub fn new(step_interval: Duration) -> Self {
        let mut x = Self::default();
        x.step_interval = step_interval;
        x
    }

    pub fn calculate_step(&mut self, current_time: Instant) {
        if (current_time - self.prev_time) >= self.step_interval {
            self.should_step = true;
            self.prev_time = current_time;
        }
    }
}
