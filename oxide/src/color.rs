//! Colors
//!
//! Simple color types and conversions between them and primitives

#[derive(Debug, Default, Copy, Clone)]
pub struct Rgb255 {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

#[derive(Debug, Default, Copy, Clone)]
pub struct Rgba255 {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
    pub alpha: u8,
}

#[derive(Debug, Default, Copy, Clone)]
pub struct RgbFloat {
    pub red: f32,
    pub green: f32,
    pub blue: f32,
}

#[derive(Debug, Default, Copy, Clone)]
pub struct RgbaFloat {
    pub red: f32,
    pub green: f32,
    pub blue: f32,
    pub alpha: f32,
}

//
// Rgb255
//
impl Rgb255 {
    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Self { red, green, blue }
    }
}

// Rgba255 -> Rgb255
impl From<Rgba255> for Rgb255 {
    fn from(color: Rgba255) -> Self {
        Self {
            red: color.red,
            green: color.green,
            blue: color.blue,
        }
    }
}

// RgbFloat -> Rgb255
impl From<RgbFloat> for Rgb255 {
    fn from(color: RgbFloat) -> Self {
        Self {
            red: (color.red * 255.0) as u8,
            green: (color.green * 255.0) as u8,
            blue: (color.blue * 255.0) as u8,
        }
    }
}

// RgbaFloat -> Rgb255
impl From<RgbaFloat> for Rgb255 {
    fn from(color: RgbaFloat) -> Self {
        Self {
            red: (color.red * 255.0) as u8,
            green: (color.green * 255.0) as u8,
            blue: (color.blue * 255.0) as u8,
        }
    }
}

// [u8; 3] -> Rgb255
impl From<[u8; 3]> for Rgb255 {
    fn from(array: [u8; 3]) -> Self {
        Self {
            red: array[0],
            green: array[1],
            blue: array[2],
        }
    }
}

// Rgb255 -> [u8; 3]
impl From<Rgb255> for [u8; 3] {
    fn from(color: Rgb255) -> [u8; 3] {
        [color.red, color.green, color.blue]
    }
}

// [u8; 4] -> Rgb255
impl From<[u8; 4]> for Rgb255 {
    fn from(array: [u8; 4]) -> Self {
        Self {
            red: array[0],
            green: array[1],
            blue: array[2],
        }
    }
}

// Rgb255 -> [u8; 4]
impl From<Rgb255> for [u8; 4] {
    fn from(color: Rgb255) -> [u8; 4] {
        [color.red, color.green, color.blue, 255]
    }
}

// (u8, u8, u8) -> Rgb255
impl From<(u8, u8, u8)> for Rgb255 {
    fn from(tuple: (u8, u8, u8)) -> Self {
        Self {
            red: tuple.0,
            green: tuple.1,
            blue: tuple.2,
        }
    }
}

// Rgb255 -> (u8, u8, u8)
impl From<Rgb255> for (u8, u8, u8) {
    fn from(color: Rgb255) -> (u8, u8, u8) {
        (color.red, color.green, color.blue)
    }
}

// (u8, u8, u8, u8) -> Rgb255
impl From<(u8, u8, u8, u8)> for Rgb255 {
    fn from(tuple: (u8, u8, u8, u8)) -> Self {
        Self {
            red: tuple.0,
            green: tuple.1,
            blue: tuple.2,
        }
    }
}

// Rgb255 -> (u8, u8, u8, u8)
impl From<Rgb255> for (u8, u8, u8, u8) {
    fn from(color: Rgb255) -> (u8, u8, u8, u8) {
        (color.red, color.green, color.blue, 255)
    }
}

//
// Rgba255
//
impl Rgba255 {
    pub fn new(red: u8, green: u8, blue: u8, alpha: u8) -> Self {
        Self {
            red,
            green,
            blue,
            alpha,
        }
    }
}

// Rgb255 -> Rgba255
impl From<Rgb255> for Rgba255 {
    fn from(color: Rgb255) -> Self {
        Self {
            red: color.red,
            green: color.green,
            blue: color.blue,
            alpha: 255,
        }
    }
}

// RgbFloat -> Rgba255
impl From<RgbFloat> for Rgba255 {
    fn from(color: RgbFloat) -> Self {
        Self {
            red: (color.red * 255.0) as u8,
            green: (color.green * 255.0) as u8,
            blue: (color.blue * 255.0) as u8,
            alpha: 255,
        }
    }
}

// RgbaFloat -> Rgba255
impl From<RgbaFloat> for Rgba255 {
    fn from(color: RgbaFloat) -> Self {
        Self {
            red: (color.red * 255.0) as u8,
            green: (color.green * 255.0) as u8,
            blue: (color.blue * 255.0) as u8,
            alpha: (color.alpha * 255.0) as u8,
        }
    }
}

// [u8; 3] -> Rgba255
impl From<[u8; 3]> for Rgba255 {
    fn from(array: [u8; 3]) -> Self {
        Self {
            red: array[0],
            green: array[1],
            blue: array[2],
            alpha: 255,
        }
    }
}

// Rgba255 -> [u8; 3]
impl From<Rgba255> for [u8; 3] {
    fn from(color: Rgba255) -> [u8; 3] {
        [color.red, color.green, color.blue]
    }
}

// [u8; 4] -> Rgba255
impl From<[u8; 4]> for Rgba255 {
    fn from(array: [u8; 4]) -> Self {
        Self {
            red: array[0],
            green: array[1],
            blue: array[2],
            alpha: array[3],
        }
    }
}

// Rgba255 -> [u8; 4]
impl From<Rgba255> for [u8; 4] {
    fn from(color: Rgba255) -> [u8; 4] {
        [color.red, color.green, color.blue, color.alpha]
    }
}

// (u8, u8, u8) -> Rgba255
impl From<(u8, u8, u8)> for Rgba255 {
    fn from(tuple: (u8, u8, u8)) -> Self {
        Self {
            red: tuple.0,
            green: tuple.1,
            blue: tuple.2,
            alpha: 255,
        }
    }
}

// Rgba255 -> (u8, u8, u8)
impl From<Rgba255> for (u8, u8, u8) {
    fn from(color: Rgba255) -> (u8, u8, u8) {
        (color.red, color.green, color.blue)
    }
}

// (u8, u8, u8, u8) -> Rgba255
impl From<(u8, u8, u8, u8)> for Rgba255 {
    fn from(tuple: (u8, u8, u8, u8)) -> Self {
        Self {
            red: tuple.0,
            green: tuple.1,
            blue: tuple.2,
            alpha: tuple.3,
        }
    }
}

// Rgba255 -> (u8, u8, u8, u8)
impl From<Rgba255> for (u8, u8, u8, u8) {
    fn from(color: Rgba255) -> (u8, u8, u8, u8) {
        (color.red, color.green, color.blue, color.alpha)
    }
}

//
// RgbFloat
//
impl RgbFloat {
    pub fn new(red: f32, green: f32, blue: f32) -> Self {
        Self { red, green, blue }
    }
}

// RgbaFloat -> RgbFloat
impl From<RgbaFloat> for RgbFloat {
    fn from(color: RgbaFloat) -> Self {
        Self {
            red: color.red,
            green: color.green,
            blue: color.blue,
        }
    }
}

// Rgb255 -> RgbFloat
impl From<Rgb255> for RgbFloat {
    fn from(color: Rgb255) -> Self {
        Self {
            red: color.red as f32 / 255.0,
            green: color.green as f32 / 255.0,
            blue: color.blue as f32 / 255.0,
        }
    }
}

// Rgba255 -> RgbFloat
impl From<Rgba255> for RgbFloat {
    fn from(color: Rgba255) -> Self {
        Self {
            red: color.red as f32 / 255.0,
            green: color.green as f32 / 255.0,
            blue: color.blue as f32 / 255.0,
        }
    }
}

// [f32; 3] -> RgbFloat
impl From<[f32; 3]> for RgbFloat {
    fn from(array: [f32; 3]) -> Self {
        Self {
            red: array[0],
            green: array[1],
            blue: array[2],
        }
    }
}

// RgbFloat -> [f32; 3]
impl From<RgbFloat> for [f32; 3] {
    fn from(color: RgbFloat) -> [f32; 3] {
        [color.red, color.green, color.blue]
    }
}

// [f32; 4] -> RgbFloat
impl From<[f32; 4]> for RgbFloat {
    fn from(array: [f32; 4]) -> Self {
        Self {
            red: array[0],
            green: array[1],
            blue: array[2],
        }
    }
}

// RgbFloat -> [f32; 4]
impl From<RgbFloat> for [f32; 4] {
    fn from(color: RgbFloat) -> [f32; 4] {
        [color.red, color.green, color.blue, 1.0]
    }
}

// (f32, f32, f32) -> RgbFloat
impl From<(f32, f32, f32)> for RgbFloat {
    fn from(tuple: (f32, f32, f32)) -> Self {
        Self {
            red: tuple.0,
            green: tuple.1,
            blue: tuple.2,
        }
    }
}

// RgbFloat -> (f32, f32, f32)
impl From<RgbFloat> for (f32, f32, f32) {
    fn from(color: RgbFloat) -> (f32, f32, f32) {
        (color.red, color.green, color.blue)
    }
}

// (f32, f32, f32, f32) -> RgbFloat
impl From<(f32, f32, f32, f32)> for RgbFloat {
    fn from(tuple: (f32, f32, f32, f32)) -> Self {
        Self {
            red: tuple.0,
            green: tuple.1,
            blue: tuple.2,
        }
    }
}

// RgbFloat -> (f32, f32, f32, f32)
impl From<RgbFloat> for (f32, f32, f32, f32) {
    fn from(color: RgbFloat) -> (f32, f32, f32, f32) {
        (color.red, color.green, color.blue, 1.0)
    }
}

//
// RgbaFloat
//
impl RgbaFloat {
    pub fn new(red: f32, green: f32, blue: f32, alpha: f32) -> Self {
        Self {
            red,
            green,
            blue,
            alpha,
        }
    }
}

// RgbFloat -> RgbaFloat
impl From<RgbFloat> for RgbaFloat {
    fn from(color: RgbFloat) -> Self {
        Self {
            red: color.red,
            green: color.green,
            blue: color.blue,
            alpha: 1.0,
        }
    }
}

// Rgb255 -> RgbaFloat
impl From<Rgb255> for RgbaFloat {
    fn from(color: Rgb255) -> Self {
        Self {
            red: color.red as f32 / 255.0,
            green: color.green as f32 / 255.0,
            blue: color.blue as f32 / 255.0,
            alpha: 1.0,
        }
    }
}

// Rgba255 -> RgbFloat
impl From<Rgba255> for RgbaFloat {
    fn from(color: Rgba255) -> Self {
        Self {
            red: color.red as f32 / 255.0,
            green: color.green as f32 / 255.0,
            blue: color.blue as f32 / 255.0,
            alpha: color.alpha as f32 / 255.0,
        }
    }
}

// [f32; 3] -> RgbaFloat
impl From<[f32; 3]> for RgbaFloat {
    fn from(array: [f32; 3]) -> Self {
        Self {
            red: array[0],
            green: array[1],
            blue: array[2],
            alpha: 1.0,
        }
    }
}

// RgbaFloat -> [f32; 3]
impl From<RgbaFloat> for [f32; 3] {
    fn from(color: RgbaFloat) -> [f32; 3] {
        [color.red, color.green, color.blue]
    }
}

// [f32; 4] -> RgbaFloat
impl From<[f32; 4]> for RgbaFloat {
    fn from(array: [f32; 4]) -> Self {
        Self {
            red: array[0],
            green: array[1],
            blue: array[2],
            alpha: array[3],
        }
    }
}

// RgbaFloat -> [f32; 4]
impl From<RgbaFloat> for [f32; 4] {
    fn from(color: RgbaFloat) -> [f32; 4] {
        [color.red, color.green, color.blue, color.alpha]
    }
}

// (f32, f32, f32) -> RgbaFloat
impl From<(f32, f32, f32)> for RgbaFloat {
    fn from(tuple: (f32, f32, f32)) -> Self {
        Self {
            red: tuple.0,
            green: tuple.1,
            blue: tuple.2,
            alpha: 1.0,
        }
    }
}

// RgbaFloat -> (f32, f32, f32)
impl From<RgbaFloat> for (f32, f32, f32) {
    fn from(color: RgbaFloat) -> (f32, f32, f32) {
        (color.red, color.green, color.blue)
    }
}

// (f32, f32, f32, f32) -> RgbaFloat
impl From<(f32, f32, f32, f32)> for RgbaFloat {
    fn from(tuple: (f32, f32, f32, f32)) -> Self {
        Self {
            red: tuple.0,
            green: tuple.1,
            blue: tuple.2,
            alpha: tuple.3,
        }
    }
}

// RgbaFloat -> (f32, f32, f32, f32)
impl From<RgbaFloat> for (f32, f32, f32, f32) {
    fn from(color: RgbaFloat) -> (f32, f32, f32, f32) {
        (color.red, color.green, color.blue, color.alpha)
    }
}
