use snafu::Snafu;

pub type OxideBuilderResult<T, E = OxideBuilderError> = Result<T, E>;

#[derive(Debug, Snafu)]
pub enum OxideBuilderError {
    #[snafu(display("Failed to create ron config from string:\n\ncaused by:\n\t{}", msg))]
    Ron { msg: String },
    #[snafu(display("Failed to create Renderer:\n\ncaused by:\n\t{}", msg))]
    Renderer { msg: String },
    #[snafu(display("Failed to create Glutin surface:\n\ncaused by:\n\t{}", msg))]
    Glutin { msg: String },
}

impl From<ron::error::Error> for OxideBuilderError {
    fn from(error: ron::error::Error) -> Self {
        Self::Ron {
            msg: error.to_string(),
        }
    }
}

impl From<RendererError> for OxideBuilderError {
    fn from(error: RendererError) -> Self {
        Self::Renderer {
            msg: error.to_string(),
        }
    }
}

impl From<luminance_glutin::GlutinError> for OxideBuilderError {
    fn from(error: luminance_glutin::GlutinError) -> Self {
        Self::Glutin {
            msg: error.to_string(),
        }
    }
}

pub type RendererResult<T, E = RendererError> = Result<T, E>;

#[derive(Debug, Snafu)]
pub enum RendererError {
    #[snafu(display("Failed to create framebuffer:\n\ncaused by:\n\t{}", msg))]
    FrameBuffer { msg: String },
    #[snafu(display(
        "There was a problem with the shader Program:\n\ncaused by:\n\t{}",
        msg
    ))]
    ProgramError { msg: String },
}

impl From<luminance::framebuffer::FramebufferError> for RendererError {
    fn from(error: luminance::framebuffer::FramebufferError) -> Self {
        Self::FrameBuffer {
            msg: error.to_string(),
        }
    }
}

impl From<luminance::shader::ProgramError> for RendererError {
    fn from(error: luminance::shader::ProgramError) -> Self {
        Self::ProgramError {
            msg: error.to_string(),
        }
    }
}
